# 一些开发中用到的Toolkit

## MathLive 

这是一个MathLive的Vue包装，用于渲染和编辑数学公式。 

[https://mathlive.io/](https://mathlive.io/)

---


## html2canvas

html2canvas 直接在用户浏览器上拍摄网页或其中一部分的“屏幕截图”。屏幕截图基于DOM，因此可能无法真实表示100％的准确度，因为它无法生成实际的屏幕截图，而是根据页面上的可用信息构建屏幕截图。

[http://html2canvas.hertzen.com/getting-started](http://html2canvas.hertzen.com/getting-started)

---

## dom-to-image

dom-to-image是一个库，可以将任意DOM节点转换为用JavaScript编写的矢量（SVG）或光栅（PNG或JPEG）图像。它基于Paul Bakaus的domvas进行了完全重写，并修复了一些错误，并添加了一些新功能（如Web字体和图像支持）。

[https://github.com/tsayen/dom-to-image#readme](https://github.com/tsayen/dom-to-image#readme)

---

## OfficeWeb365

专注于Office文档在线预览及PDF文档在线预览云服务，包括Microsoft Word文档在线预览、Excel表格在线预览、Powerpoint演示文档在线预览，WPS文字处理、WPS表格、WPS演示及Adobe PDF文档在线预览。

[https://officeweb365.com/](https://officeweb365.com/)